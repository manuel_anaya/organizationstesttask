# Organizations Test Task

.NET WebApi

### Installation
In order to get the test running please follow these steps:

* Clone the repository or Download the source
* Run the SQL query ***StartupQuery.sql*** located in the root folder. The result is a new database called "ManuelTestApp" Containing:
    * Table: Organization
    * Stored Procedure: SearchOrganizations
    * Stored procedure: TruncateOrganizationsTable
* Open the project in Visual Studio, it contains 3 projects:
    * AppTests - Validation for sample schemas and organizations insert/search
    * WebAPI - Web API that offers create, search and delete organizations
	* WebUI - Optional interface to interact with the API
	 
* Update each project's database connection strings to be able to operate correctly. You are able to find them under the Configuration/connectionStrings settings node. Search for "TestAppConnectionString" and update it to reflect your local SQL server connection.
    * AppTests/app.config
    * WebApi/Web.config
    * WebUi/Web.config
* From visual studio open the project restore the Nuget packages for the solution
* Build the Solution
* Start the WebApi (default starting url address is http://localhost:51070 )
* In case you change the WebApi default's address (http://localhost:51070) then Update the WebUi Web.config's file to point to the WebApi's reachable URL.

### WebAPI Usage

All operations for INSERT. SEARCH and DELETE will be reachable at the http://[WEBAPIURL]/api/organizations (default http://localhost:51070/api/organizations) where depending the type of request will perform the operation, as described below:

### [POST] 

Use a POST request to /api/organizations for sending organizations JSON payload to create organizations. The endpoint will validate the input payload against this schema

```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "additionalProperties": false,
  "anyOf": [
    {"required": ["org_name"]}
   ],
  "properties": {
    "org_name": {
      "type": "string"      
    },
    "daughters": {
      "type": "array",
      "items": {"$ref": "#"}
    }
  },
  "required": ["org_name"]
}
```
* Specify in the Request header that the Content-Type is application/json
* Specify in the Request Body the provided schema valid Organizations JSON
* If the JSON schema is valid and the organizations table contains any rows, it will truncate the table using the stored procedure so a new organization structure is created.
* The organizations then will be inserted into the Organization table in ManuelTestApp database using the WebApi/Services/OrganizationsService
* Insert splits the query into batches of 1000 records. This to be in compliant with the insert constructor method's limitation. 
* A transaction for insertion is in place and in case of an error it will be rolled back. This to ensure data integrity: all or none.
* If the response is successful you will receive a message like: "Success! <total_inserted_rows_number> rows were created"
* Response code 200 is returned if transaction was OK
* Response 400 is returned for Bad Request
* Response 500 is returned for Internal Server Error
* Feedback messages are provided with each response

### [GET]

Use a GET request to perform a search. The format is: 
/api/organizations?***Name***=[Required]&***pageSize***=[optional]&***pageNo***=[optional]

The ***Name*** parameter is required but it can be left empty, for example:  
```
    http://localhost:51070/api/organizations?Name=
```
Is a valid request

* If not provided, default pageSize is 100 and default pageNo is 1
* Response body contains the JSON formatted results
* The response header contains as well:
```
Content-Type →application/json; charset=utf-8
X-Paging-PageCount → How many pages were generated given the Page Size and Total Records
X-Paging-PageNo → The current page of the results being displayed
X-Paging-PageSize →The page size
X-Paging-TotalRecordCount → Total found records matching search criteria
```


### [DELETE]
Use a DELETE Request to the /api/organizations in order to truncate the Organizations table. As mentioned before, if a POST with a new organizations JSON is made, the service will automatically truncate the table for us. 

### WEB UI
Initiate the WebUi after the WebApi is running.
* if the organizations table is not populated yet, the home page will allow to upload an organization JSON. The used JSON schema to validate the input is displayed on screen.
* In case of an error a floating window with the descriptive message will appear on screen. Messages can be discarded using the "OK" button.
* Once the data has been successfully created, the user will be redirected to the search page. 
* The search page offers:
    * Search by provided organization name
    * Filter to show or not parents, sisters and daughters
    * Specify the page size using a drop down menu
    * Pagination with: page buttons, next page, previous page and Jump to Page
    * Possibility to truncate the organizations table. Once this operation is completed, the page will automatically redirect to Home/Index.

WebUI was created using .NET MVC with Razor, JQuery and Bootstrap 4. 

### AppTests
Include a few tests on services operations. It uses Microsoft Visual Studio Test Tools. 

### Logging
Log4net is used to trace the different types of operations: log.Error for exceptions and log.Info for activity tracking.