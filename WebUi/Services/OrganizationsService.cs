﻿using System.Linq;
using WebUi.Helpers;
using WebUi.Models;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System;
using System.Data;

namespace WebUi.Services
{
    public class OrganizationsService
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string organizationsQuery = "INSERT Organization \nVALUES\n";

        private string _connectionString;

        public OrganizationsService(string connectionString)
        {
            _connectionString = connectionString;
        }

        public OrganizationsViewModel Search(OrganizationsSearch search, Pager pager)
        {
            var totalCount = 0;

            var results = PerformSearch(search, pager);

            if (results.Any())
                totalCount = results.First().TotalCount.Value;
            search.Page = pager.CurrentPage;
            var newPager = SetPagerAfterSearchResults(search, totalCount);
            newPager.CurrentPage = pager.CurrentPage;

            var searchResults = new OrganizationsViewModel { Organizations = results, Pager = newPager, Search = search };

            return searchResults;
        }

        private List<SearchOrganizationsResult> PerformSearch(OrganizationsSearch search, Pager pager)
        {
            var results = new List<SearchOrganizationsResult>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                var proc = "SearchOrganizations";
                using (SqlCommand cmd = new SqlCommand(proc, con))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@Search", SqlDbType.VarChar).Value = search.Name != null ? search.Name.Trim() : string.Empty;
                        cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = pager.CurrentPage;
                        cmd.Parameters.Add("@ShowParent", SqlDbType.Bit).Value = search.ShowParent;
                        cmd.Parameters.Add("@ShowSisters", SqlDbType.Bit).Value = search.ShowSisters;
                        cmd.Parameters.Add("@ShowDaughters", SqlDbType.Bit).Value = search.ShowDaughters;
                        cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = search.RowsPerPage;
                        cmd.Parameters.Add("@SortOrder", SqlDbType.VarChar).Value = search != null ? search.SortDirection ?? OrganizationsSearch.DefaultSortDirection : string.Empty;

                        con.Open();

                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                results.Add(new SearchOrganizationsResult { RelatedNode = reader[0].ToString(), Relationship = reader[1].ToString(), TotalCount = int.Parse(reader[2].ToString()) });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        _log.Fatal($"An error has occurred while executing an SQL procedure:\"{proc}\", message: {ex.Message}");
                        throw;
                    }
                }
            }
            return results;
        }

        public bool IsDatabaseInitialized()
        {
            var result = false;
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                var query = "SELECT COUNT(*) FROM Organization";
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    try
                    {
                        con.Open();
                        result = (int)cmd.ExecuteScalar() > 0;
                    }
                    catch (Exception ex)
                    {
                        _log.Fatal($"An error has occurred while executing an SQL query:\"{query}\", message: {ex.Message}");
                        throw;
                    }
                }

            }
            return result;
        }

        public int SaveOrganizations(OrganizationPayload payload)
        {
            var result = 0;

            GenerateOrganizationsQuery(payload, null, 1, "/");
            organizationsQuery = organizationsQuery.Substring(0, organizationsQuery.LastIndexOf(",")) + ";";

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(organizationsQuery, con))
                {
                    try
                    {
                        con.Open();
                        result = (int)cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        _log.Fatal($"An error has occurred while executing an SQL query:\"{organizationsQuery}\", message: {ex.Message}");
                        throw;
                    }
                    finally
                    {
                        organizationsQuery = "INSERT Organization \nVALUES\n";
                    }
                }
            }

            return result;
        }

        private void GenerateOrganizationsQuery(OrganizationPayload node, OrganizationPayload parent, int level, string hierarchy)
        {
            var computedHierarchy = $"{hierarchy}{level}/";
            organizationsQuery += $"('{computedHierarchy}', '{node.OrganizationName}'),\n";

            level++;
            if (node.Daughters != null)
            {
                var childCounter = 0;
                foreach (var childNode in node.Daughters)
                {
                    childCounter++;
                    GenerateOrganizationsQuery(childNode, node, childCounter, computedHierarchy);
                }
            }
        }

        public Pager SetPagerAfterSearchResults<T>(T search, int totalCount) where T : BaseSearch
        {
            var pager = new Pager();
            pager.ItemsCount = totalCount;
            pager.SetPageCount(totalCount, search.RowsPerPage);
            pager.CurrentPage = search.Page;
            pager.PagingStartPage = 1;
            pager.SetPageNumbers();
            return pager;
        }

    }
}