﻿namespace WebUi.Helpers
{
    public abstract class BaseSearch
    {

        private string _defaultSortBy = "Name";
        private int _rowsPerPage = 5;

        public virtual string DefaultSortBy
        {
            get { return _defaultSortBy; }
        }

        public static string DefaultSortDirection = "ASC";
        public static string DescendingSortDirection = "DESC";

        #region Searching Properties

        public string SortBy { get; set; }

        public string SortDirection { get; set; }

        public int Page { get; set; }

        public int RowsPerPage { get => _rowsPerPage; set => _rowsPerPage = value; }

        #endregion
    }
}