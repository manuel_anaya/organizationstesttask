﻿using System.Collections.Generic;
using WebUi.Helpers;
using WebUi.Models;

namespace WebUi.Models
{
    public class OrganizationsViewModel
    {
        public OrganizationsSearch Search { get; set; }

        public List<SearchOrganizationsResult> Organizations { get; set; }

        public Pager Pager { get; set; }

        public OrganizationsViewModel GetChangePageModel(OrganizationsSearch search, Pager pager, int currentPage)
        {
            pager.CurrentPage = currentPage;
            return new OrganizationsViewModel
            {
                Search = search,
                Pager = pager
            };
        }

        public OrganizationsViewModel GetChangeSearchModel(OrganizationsSearch search, Pager pager, string name, bool showParent, bool showSisters, bool showDaughters)
        {
            search.Name = name;
            search.ShowParent = showParent;
            search.ShowSisters = showSisters;
            search.ShowDaughters = showDaughters;
            return new OrganizationsViewModel
            {
                Search = search,
                Pager = pager
            };
        }
    }
}