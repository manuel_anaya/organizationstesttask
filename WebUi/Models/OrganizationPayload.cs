﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace WebUi.Models
{
    public class OrganizationPayload
    {
        [JsonProperty("org_name")]
        public string OrganizationName { get; set; }

        [JsonProperty("daughters")]
        public List<OrganizationPayload> Daughters { get; set; }
    }
}
