namespace WebUi.Models
{
    using System;
    
    public partial class SearchOrganizationsResult
    {
        public string RelatedNode { get; set; }
        public string Relationship { get; set; }
        public Nullable<int> TotalCount { get; set; }
    }
}
