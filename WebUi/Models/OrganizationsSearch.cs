﻿using WebUi.Helpers;

namespace WebUi.Models
{
    public class OrganizationsSearch : BaseSearch
    {
        private bool _showParent = true;
        private bool _showSisters = true;
        private bool _showDaughters = true;
        //private int _rowsPerPage = 2;

        public string Name { get; set; }

        public bool ShowParent
        {
            get { return _showParent; }
            set { _showParent = value; }
        }

        public bool ShowSisters { get { return _showSisters; }
            set { _showSisters = value; }
        }
        public bool ShowDaughters {
            get { return _showDaughters; }
            set { _showDaughters = value; }
        }

        //public int RowsPerPage
        //{
        //    get { return _rowsPerPage; }
        //    set { _rowsPerPage = value; }
        //}
    }
}