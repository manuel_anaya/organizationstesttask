﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Web.Mvc;
using WebUi.Services;

namespace WebUi.Controllers
{
    public class HomeController : Controller
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        string _apiPushUrl;
        string _responsiveCheck;
        Stopwatch _watch;

        private JSchema _schema;

        private string _schemaJson = @"{
              '$schema': 'http://json-schema.org/draft-04/schema#',
              'type': 'object',
              'additionalProperties': false,
              'anyOf': [
  	            {'required': ['org_name']}
              ],
              'properties': {
                'org_name': {
                  'type': 'string'      
                },
                'daughters': {
                  'type': 'array',
                  'items': {'$ref': '#'}
                }
              },
              'required': ['org_name']
            }";

        private OrganizationsService _organizationsService;

        public HomeController()
        {
            _organizationsService = new OrganizationsService(ConfigurationManager.ConnectionStrings["TestAppConnectionString"].ConnectionString);
            _schema = JSchema.Parse(_schemaJson);
            _apiPushUrl = Convert.ToString(ConfigurationManager.AppSettings["ApiUrl"]);
            _responsiveCheck = Convert.ToString(ConfigurationManager.AppSettings["ResponsiveCheckUrl"]);            
            _watch = new Stopwatch();
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            var isApiAvailable = false;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(_responsiveCheck);
            httpWebRequest.Method = "GET";

            // check if the API is up
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    if (httpResponse.StatusCode == HttpStatusCode.OK)
                    {
                        isApiAvailable = true;
                    }
                    else
                    {
                        _log.Error($"An error has ocurred while posting the payload, got: {httpResponse}");

                        ViewBag.ErrorTitle = "Server Error";
                        ViewBag.ErrorMessage = $"There seems to be an error while processing your request. Please ensure the API is reachable.";
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error($"An error has ocurred while accessing the API: {ex.Message}");
                ViewBag.ErrorTitle = "API not accessible";
                ViewBag.ErrorMessage = $"An error has ocurred while accessing the API: {ex.Message}";
            }

            //check if db is accessible and set up
            if (isApiAvailable)
            {
                try
                {
                    // if the data has been loaded already then redirect to the search page
                    if (_organizationsService.IsDatabaseInitialized())
                    {                    
                        Response.Redirect(Url.Action("Index", "Search"));
                    }
                }
                catch (Exception ex)
                {
                    _log.Error($"An error has ocurred while posting the verifying database initialization: {ex.Message}");
                    ViewBag.ErrorTitle = "Error accessing Database";
                    ViewBag.ErrorMessage = $"Please check the database connection string in the app.config file and the startup script has been run before proceeding. Error: {ex.Message}";
                }
            }           

            return View();
        }

        [HttpPost]
        public ActionResult Index(string jsoninput)
        {
            ViewBag.JsonInput = jsoninput;

            if (string.IsNullOrEmpty(jsoninput))
            {
                ViewBag.ErrorTitle = "Invalid Json";
                ViewBag.ErrorMessage = "No payload was entered, please enter a valid one.";
            }
            else
            {
                ViewBag.Error = "";
                JObject organizations = null;
                try
                {
                    organizations = JObject.Parse(jsoninput);
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorTitle = "Invalid Schema";
                    ViewBag.ErrorMessage = $"The following error was found while parsing the given input: {ex.Message}";
                }
                // validate input against json schema
                if (organizations != null && organizations.IsValid(_schema))
                {
                    try
                    {
                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(_apiPushUrl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Method = "POST";

                        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                        {
                            streamWriter.Write(jsoninput);
                            streamWriter.Flush();
                            streamWriter.Close();
                        }

                        _watch.Reset();
                        _watch.Start();
                        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        _log.Info("HttpWebResponse is received");
                        _watch.Stop();
                        if (httpResponse.StatusCode == HttpStatusCode.OK)
                        {
                            Response.Redirect(Url.Action("Index", "Search"));
                        }
                        else
                        {
                            _log.Error($"An error has ocurred while posting the payload, got: {httpResponse}");

                            ViewBag.ErrorTitle = "Server Error";
                            ViewBag.ErrorMessage = $"There seems to be an error while processing your request. Please ensure the API is reachable.";
                        }
                    }
                    catch (Exception ex)
                    {
                        _log.Error($"An error has ocurred while posting the info: {ex.Message}");
                        ViewBag.ErrorTitle = "Error while sending data to the API";
                        ViewBag.ErrorMessage = $"An error has ocurred while posting the info: {ex.Message}";
                    }
                }
                else if (string.IsNullOrEmpty(ViewBag.ErrorTitle) && string.IsNullOrEmpty(ViewBag.ErrorMessage))
                { // check we don't override previous error messages
                    ViewBag.ErrorTitle = "Invalid Schema";
                    ViewBag.ErrorMessage = "The given JSON is not compliant with the given schema, please enter a valid one.";
                }
            }
            //validate and post to webapi if ok
            return View();
        }
    }
}