﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WebUi.Models;
using WebUi.Helpers;
using WebUi.Services;
using System.Configuration;
using System.Net;

namespace WebUi.Controllers
{
    public class SearchController : Controller
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string _truncateUrl;
        private Pager _pager
        {
            get
            {
                if (Session["pager"] == null)
                    Session["pager"] = new Pager();

                return (Pager)Session["pager"];
            }
            set
            {
                Session["pager"] = value;
            }
        }

        private OrganizationsSearch _search
        {
            get
            {
                if (Session["search"] == null)
                    Session["search"] = new OrganizationsSearch();

                return (OrganizationsSearch)Session["search"];
            }
            set
            {
                Session["search"] = value;
            }
        }

        private OrganizationsService _organizationsService;
        //private OrganizationsSearch _search;
        //private Pager _pager;


        public SearchController()
        {
            _organizationsService = new OrganizationsService(ConfigurationManager.ConnectionStrings["TestAppConnectionString"].ConnectionString);
            _truncateUrl = Convert.ToString(ConfigurationManager.AppSettings["TruncateUrl"]);
        }

        // GET: Search
        public ActionResult Index()
        {
            var model = new OrganizationsViewModel
            {
                Pager = new Pager(),
                Organizations = new List<SearchOrganizationsResult>(),
                Search = new OrganizationsSearch()
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(OrganizationsSearch search)
        {
            _search = search;
            var model = _organizationsService.Search(search, new Pager());
            return View(model);
        }

        // update search (paginate, sort, etc). Returns partial with updated results.
        [HttpPost]
        public ActionResult UpdateSearch(OrganizationsViewModel search)
        {
            var model = new OrganizationsViewModel();
            try
            {
                model = _organizationsService.Search(_search, new Pager());
                _pager = model.Pager;                
            }
            catch(Exception ex)
            {
                ViewBag.ErrorTitle = "Error accessing Database";
                ViewBag.ErrorMessage = $"Please check the database connection string in the app.config file and the startup script has been run before proceeding. Error: {ex.Message}";
            }
            return PartialView("_SearchResults", model);
        }

        [HttpPost]
        public ActionResult ChangePage(int page)
        {
            var model = new OrganizationsViewModel();
            try
            {
                _pager.CurrentPage = page;
                model = _organizationsService.Search(_search, _pager);
                _pager = model.Pager;

                return PartialView("_SearchResults", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorTitle = "Error accessing Database";
                ViewBag.ErrorMessage = $"Please check the database connection string in the app.config file and the startup script has been run before proceeding. Error: {ex.Message}";
            }
            return PartialView("_SearchResults", model);
        }

        [HttpPost]
        public ActionResult Truncate()
        {
            try
            {
                using (var client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    var data = "=secret";
                    var result = client.UploadString(_truncateUrl, "POST", data);
                    if (result == "\"ok\"")
                    {
                        return new HttpStatusCodeResult(200);
                    }
                    else
                    {
                        _log.Error($"An error has ocurred while  attempting to truncate table, got: {result}");
                        return new HttpStatusCodeResult(500, $"An error has ocurred while  attempting to truncate table, got: {result}");
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error($"An error has ocurred while posting the info: {ex.Message}");
                return new HttpStatusCodeResult(500, $"An error has ocurred while posting the info: {ex.Message}");
            }

        }
    }
}