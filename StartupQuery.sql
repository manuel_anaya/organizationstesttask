IF  NOT EXISTS (SELECT * FROM sys.databases WHERE name = N'ManuelTestApp')
    BEGIN
        CREATE DATABASE [ManuelTestApp]
    END;
GO

USE ManuelTestApp
GO

CREATE TABLE Organization 
   (  
    [Level] hierarchyid,  
    [OrgLevel] as [Level].GetLevel(),   
    [Name] nvarchar(50) NOT NULL  
   )  
GO  

CREATE CLUSTERED INDEX [IX_BreadthFirst]
	ON Organization(OrgLevel,[Level])  
GO  


CREATE PROCEDURE SearchOrganizations
	(
		@Search NVARCHAR(150) = NULL,
		@PageNo INT = 1,
		@ShowParent BIT = 1,
		@ShowSisters BIT = 1,
		@ShowDaughters BIT = 1,
		@PageSize INT = 10,
		@SortOrder NVARCHAR(4) = 'ASC'
	)
AS BEGIN
	SET NOCOUNT ON;

	DECLARE @TotalCount INT = 0;

	DECLARE @organizations table
	(
		CurrentNode nvarchar(150) NOT NULL, 
		RelatedNode nvarchar(150) NOT NULL, 
		Relationship nvarchar(150) NOT NULL
	);

	-- parents
	IF(@ShowParent = 1)
	BEGIN
		insert into @organizations
		select parent.[Name], child.[Name], 'parent' 
		FROM Organization child inner join Organization parent on parent.[Level].GetAncestor(1) = child.[Level]
		WHERE parent.[Name] LIKE '%' + @Search + '%';
	END

	-- daughters
	IF(@ShowDaughters = 1)
	BEGIN
		insert into @organizations
		select parent.[Name], child.[Name], 'daughter'  
		FROM Organization parent inner join Organization child on parent.[Level] = child.[Level].GetAncestor(1) 
		WHERE parent.[Name] LIKE '%' + @Search + '%';
	END

	----sisters
	IF(@ShowSisters = 1)
	BEGIN
		insert into @organizations
		select parent.[Name], child.[Name], 'sister'  
		FROM Organization parent inner join Organization child on parent.[Level].GetAncestor(1) = child.[Level].GetAncestor(1) 
		WHERE parent.[Name]  LIKE '%' + @Search + '%' AND parent.[Level] <> child.[Level]
	END


	DECLARE @organizationResults table
	(
		RelatedNode nvarchar(150) NOT NULL, 
		Relationship nvarchar(150) NOT NULL
	);

	INSERT INTO @organizationResults SELECT DISTINCT RelatedNode, Relationship FROM @organizations

	SET @TotalCount = (SELECT COUNT(*) FROM @organizationResults);
	
	SELECT RelatedNode, Relationship, @TotalCount as 'TotalCount'
	FROM @organizationResults
	ORDER BY RelatedNode
	OFFSET @PageSize * (@PageNo - 1) ROWS
    FETCH NEXT @PageSize ROWS ONLY
	
END

GO

CREATE PROCEDURE TruncateOrganizationsTable
AS
BEGIN
	TRUNCATE TABLE [dbo].[Organization]
END
GO