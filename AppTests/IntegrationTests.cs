﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApi.Services;
using WebUi.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using WebUi.Models;
using WebUi.Helpers;
using System.Linq;
using System.Configuration;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;

namespace AppTests
{
    [TestClass]
    public class IntegrationTests
    {
        private WebApi.Services.OrganizationsService _apiService;
        private WebUi.Services.OrganizationsService _webService;

        private string _schemaJson = @"{
              '$schema': 'http://json-schema.org/draft-04/schema#',
              'type': 'object',
              'additionalProperties': false,
              'properties': {
                'org_name': {
                  'type': 'string'
                },
                'daughters': {
                  'type': 'array',
                  'items': {'$ref': '#'}
                }
              }
            }";
        private JSchema _schema;

        [TestInitialize]
        public void Initialize()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["TestAppConnectionString"].ConnectionString;

            _apiService = new WebApi.Services.OrganizationsService(connectionString);
            _webService = new WebUi.Services.OrganizationsService(connectionString);

            _schema = JSchema.Parse(_schemaJson);
        }

        [TestMethod]
        public void test_json_is_compliant_with_schema()
        {
            var payload = @"
                {
                   ""org_name"":""Paradise Island"",
                   ""daughters"":[
                      {
                         ""org_name"":""Banana tree"",
                         ""daughters"":[
                            {
                               ""org_name"":""Yellow Banana""
                            },
                            {
                               ""org_name"":""Brown Banana""
                            },
                            {
                               ""org_name"":""Black Banana""
                            }
                         ]
                      },
                      {
                         ""org_name"":""Big banana tree"",
                         ""daughters"":[
                            {
                               ""org_name"":""Yellow Banana""
                            },
                            {
                               ""org_name"":""Brown Banana""
                            },
                            {
                               ""org_name"":""Green Banana""
                            },
                            {
                               ""org_name"":""Black Banana"",
                               ""daughters"":[
                                  {
                                     ""org_name"":""Phoneutria Spider""
                                  }
                               ]
                            }
                         ]
                      }
                   ]
                }";

            var organizations = JObject.Parse(payload);
            Assert.IsTrue(organizations.IsValid(_schema));

            payload = @"
            {
                ""org_name"":""Paradise Island"",
                ""daughters"":[
                    {
                        ""org_name1"":""Banana tree"",
                        ""daughters"":[]
                }
                ]
            }";

            organizations = JObject.Parse(payload);
            Assert.IsFalse(organizations.IsValid(_schema));


            payload = @"
            {
               ""org_name"":123,
               ""daughters"":[]
            }";

            organizations = JObject.Parse(payload);
            Assert.IsFalse(organizations.IsValid(_schema));
        }

        [TestMethod]
        public void test_payload_insert_is_successful()
        {
            var payload = @"
                {
                   ""org_name"":""Paradise Island"",
                   ""daughters"":[
                      {
                         ""org_name"":""Banana tree"",
                         ""daughters"":[
                            {
                               ""org_name"":""Yellow Banana""
                            },
                            {
                               ""org_name"":""Brown Banana""
                            },
                            {
                               ""org_name"":""Black Banana""
                            }
                         ]
                      },
                      {
                         ""org_name"":""Big banana tree"",
                         ""daughters"":[
                            {
                               ""org_name"":""Yellow Banana""
                            },
                            {
                               ""org_name"":""Brown Banana""
                            },
                            {
                               ""org_name"":""Green Banana""
                            },
                            {
                               ""org_name"":""Black Banana"",
                               ""daughters"":[
                                  {
                                     ""org_name"":""Phoneutria Spider""
                                  }
                               ]
                            }
                         ]
                      }
                   ]
                }";

            var organizations = JsonConvert.DeserializeObject<WebApi.Models.OrganizationPayload>(payload);

            //Big banana tree and Banana tree
            Assert.AreEqual(organizations.Daughters.Count, 2);
            var bigBananaTree = organizations.Daughters.Find(x=>x.OrganizationName == "Big banana tree");
            var bananaTree = organizations.Daughters.Find(x => x.OrganizationName == "Banana tree");

            Assert.AreEqual(bigBananaTree.Daughters.Count, 4);
            Assert.AreEqual(bananaTree.Daughters.Count, 3);

            //reset organizations table
            if (_apiService.IsDatabaseInitialized())
            {
                _apiService.TruncateOrganizations();
            }

            //database is now clear and not initialized
            Assert.IsFalse(_apiService.IsDatabaseInitialized());

            // upload test payload
            _apiService.SaveOrganizations(organizations);

            //organizations are now uploaded
            Assert.IsTrue(_apiService.IsDatabaseInitialized());

            var search = new OrganizationsSearch { Name = "black", RowsPerPage= 20 };
            var pager = new Pager();
            //verify all 11 organizations have been uploaded
            var results = _webService.Search(search, pager);

            Assert.AreEqual(results.Organizations.Count, 6);
            Assert.AreEqual(results.Organizations.First().TotalCount, 6);
            Assert.IsTrue(results.Organizations.Any(x => x.RelatedNode == "Phoneutria Spider" && x.Relationship == "daughter"));
            Assert.IsTrue(results.Pager.PageCount == 1);

            search.RowsPerPage = 2; //new OrganizationsSearch { Name = "black", RowsPerPage = 2 };
            results = _webService.Search(search, pager);
            Assert.IsTrue(results.Pager.PageCount == 3);

            search = new OrganizationsSearch { Name = "green", RowsPerPage = 20 };
            results = _webService.Search(search, pager);

            Assert.IsTrue(results.Organizations.Any(x => x.RelatedNode == "Big banana tree" && x.Relationship == "parent"));
            Assert.IsFalse(results.Organizations.Any(x => x.RelatedNode == "Banana tree" && x.Relationship == "parent"));
            Assert.IsTrue(results.Organizations.Count(x => x.Relationship == "sister") == 3);
            Assert.IsTrue(results.Organizations.Count(x => x.Relationship == "daughter") == 0);
        }
    }
}
