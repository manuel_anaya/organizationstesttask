﻿using System.Web.Mvc;
using System.Data.SqlClient;
using System.Configuration;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using WebApi.Models;

namespace WebApi.Services
{
    public class OrganizationsService
    {
        
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string organizationsQuery = "INSERT Organization \nVALUES\n";

        private string _connectionString;

        public OrganizationsService(string connectionString)
        {
            _connectionString = connectionString;
        }

        public bool IsDatabaseInitialized()
        {
            var result = false;
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                var query = "SELECT COUNT(*) FROM Organization";
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    try
                    {
                        con.Open();
                        result = (int)cmd.ExecuteScalar() > 0;
                    }
                    catch(Exception ex)
                    {
                        _log.Fatal($"An error has occurred while executing an SQL query:\"{query}\", message: {ex.Message}");
                        throw;
                    }
                }

            }
            return result;
        }

        List<string> insertRowStatements = new List<string>();

        public int SaveOrganizations(OrganizationPayload payload)
        {
            // keep the level of 1000 rows allowed for sql server for row constructors
            // http://blog.staticvoid.co.nz/2012/mssql_and_large_insert_statements/
            int batchInsertLimit = 2;

            var result = 0;

            GenerateOrganizationsQuery(payload, null, 1, "/");
            var batches = SplitList(insertRowStatements, batchInsertLimit);                

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();

                using (SqlTransaction transaction = con.BeginTransaction())
                {                    
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = con;
                        cmd.Transaction = transaction;
                        try
                        {
                            
                            foreach (var batch in batches)
                            {
                                cmd.CommandText = organizationsQuery + string.Join(",", batch) + ";";
                                result += (int)cmd.ExecuteNonQuery();
                            }

                            transaction.Commit();
                            _log.Info("Insertion batches has been successfuly processed");
                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                transaction.Rollback();

                                _log.Fatal($"An error has occurred while executing an SQL query:\"{organizationsQuery}\", message: {ex.Message}");
                                throw;
                            }
                            catch(Exception transactionEx)
                            {
                                _log.Fatal($"Rollback exception has occurred: {transactionEx.Message}");
                            }
                            
                        }
                    }
                }                
            }

            return result;
        }

        

        //split the insert into statements to maximum rows to be inserted
        

        private void GenerateOrganizationsQuery(OrganizationPayload node, OrganizationPayload parent, int level, string hierarchy)
        {
            var computedHierarchy = $"{hierarchy}{level}/";

            insertRowStatements.Add($"('{computedHierarchy}', '{node.OrganizationName}')\n");

            //organizationsQuery += $"('{computedHierarchy}', '{node.OrganizationName}'),\n";

            level++;
            if (node.Daughters != null)
            {
                var childCounter = 0;
                foreach (var childNode in node.Daughters)
                {
                    childCounter++;
                    GenerateOrganizationsQuery(childNode, node, childCounter, computedHierarchy);
                }
            }
        }

        public IEnumerable<List<T>> SplitList<T>(List<T> organizations, int nSize = 30)
        {
            for (int i = 0; i < organizations.Count; i += nSize)
            {
                yield return organizations.GetRange(i, Math.Min(nSize, organizations.Count - i));
            }
        }

        /// <summary>
        /// This type of operations should not be possible, but is for demo purposes
        /// </summary>
        public void TruncateOrganizations()
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                var proc = "TruncateOrganizationsTable";
                using (SqlCommand cmd = new SqlCommand(proc, con))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        con.Open();

                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        _log.Fatal($"An error has occurred while executing stored procedure TruncateOrganizationsTable, message: {ex.Message}");
                        throw;
                    }
                }
            }

        }

        public List<SearchOrganizationsResult> Search(OrganizationsSearch search)
        {
            var results = new List<SearchOrganizationsResult>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                var proc = "SearchOrganizations";
                using (SqlCommand cmd = new SqlCommand(proc, con))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@Search", SqlDbType.VarChar).Value = search.Name != null ? search.Name.Trim() : string.Empty;
                        cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = search.PageNo;
                        cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = search.PageSize;                        

                        con.Open();

                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                results.Add(new SearchOrganizationsResult { RelatedNode = reader[0].ToString(), Relationship = reader[1].ToString(), TotalCount = int.Parse(reader[2].ToString()) });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        _log.Fatal($"An error has occurred while executing an SQL procedure:\"{proc}\", message: {ex.Message}");
                        throw;
                    }
                }
            }
            return results;
        }

    }
}