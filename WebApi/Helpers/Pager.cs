﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Routing;

namespace WebApi.Helpers
{
    public class Pager
    {

        #region Properties

        // extra properties for the pages rendering
        public int ItemsCount { get; set; }
        public string SortBy { get; set; }
        public string SortDirection { get; set; }

        public static string AscendingSortDirection = "ASC";
        public static string DescendingSortDirection = "DESC";

        public string Ascending { get { return AscendingSortDirection; } }
        public string Descending { get { return DescendingSortDirection; } }

        public RouteValueDictionary SearchCriteria { get; set; }

        public bool ShowNext { get; set; }
        public bool ShowPrevious { get; set; }
        public int PreviousPage { get; set; }
        public int NextPage { get; set; }

        //public string ActionController { get; set; }

        /// <summary>
        /// The total page count 
        /// </summary>
        public int PageCount { get; set; }

        [DefaultValue(1)]
        public int CurrentPage { get; set; } = 1;

        [DefaultValue(1)]
        public int PagingStartPage { get; set; } = 1;
        [DefaultValue(10)]
        public int NumericButtonsCount { get; set; } = 10;

        public List<int> TotalPages { get; set; }

        #endregion Properties

        /// <summary>
        /// Loads the control and set its default values.
        /// </summary>
        public void LoadControl()
        {
            int firstPage;
            int lastPage;

            if (PageCount > NumericButtonsCount)
            {
                if (CurrentPage == 1)
                {
                    firstPage = CurrentPage;
                    lastPage = NumericButtonsCount;
                }
                else
                {
                    firstPage = CurrentPage - (int)Math.Ceiling(decimal.Divide(NumericButtonsCount, 2)) + 1;
                    if (firstPage < 1)
                        firstPage = 1;
                    lastPage = firstPage + NumericButtonsCount - 1;
                    if (lastPage > PageCount)
                        lastPage = PageCount;
                }
            }
            else
            {
                firstPage = 1;
                lastPage = PageCount;
            }

            if (TotalPages == null)
                TotalPages = new List<int>();
            else
                TotalPages.Clear();


            for (var i = firstPage; i <= lastPage; i++)
            {
                TotalPages.Add(i);
            }
        }

        /// <summary>
        /// Gets the next page.
        /// </summary>
        /// <returns></returns>
        public int GetNextPage()
        {
            var nextPage = CurrentPage + 1;
            if (nextPage > PageCount) nextPage = 1;
            return nextPage;
        }

        /// <summary>
        /// Gets the previous page.
        /// </summary>
        /// <returns></returns>
        public int GetPreviousPage()
        {
            var prevPage = CurrentPage - 1;
            if (prevPage < 1) prevPage = PageCount;
            return prevPage;
        }

        /// <summary>
        /// Sets the page count.
        /// </summary>
        /// <param name="numberOfRecords">The number of records.</param>
        /// <param name="recordsPerPage">The records per page.</param>
        public void SetPageCount(int numberOfRecords, int recordsPerPage)
        {
            int remainder;
            long result = Math.DivRem(numberOfRecords, recordsPerPage, out remainder);
            if ((remainder > 0) || (result == 0))
                result++;
            PageCount = (int)result;
        }

        /// <summary>
        /// Gets the page number and sets the ShowPrevious and ShowNext attributes.
        /// </summary>
        /// <returns></returns>
        public void SetPageNumbers()
        {
            if (GetPreviousPage() > 0 && GetPreviousPage() != PageCount)
            {
                PreviousPage = GetPreviousPage();
                ShowPrevious = true;
            }
            if (GetNextPage() > PageCount || GetNextPage() == 1) return;
            NextPage = CurrentPage == 0 ? 2 : GetNextPage();
            ShowNext = true;
        }
    }
}