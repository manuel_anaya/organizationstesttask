﻿using System;
using WebApi.Services;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using System.Web.Http.Results;
using System.Configuration;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json;
using WebApi.Models;
using WebApi.Helpers;
using System.Linq;
using System.Web.Http.ModelBinding;
using System.Net.Http.Headers;

namespace WebApi.Controllers
{
    public class CollectController : ApiController
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private OrganizationsService _organizationsService;

        private JSchema _schema;

        private string _schemaJson = @"{
              '$schema': 'http://json-schema.org/draft-04/schema#',
              'type': 'object',
              'additionalProperties': false,
              'anyOf': [
  	            {'required': ['org_name']}
              ],
              'properties': {
                'org_name': {
                  'type': 'string'      
                },
                'daughters': {
                  'type': 'array',
                  'items': {'$ref': '#'}
                }
              },
              'required': ['org_name']
            }";


        public CollectController()
        {
            _schema = JSchema.Parse(_schemaJson);
            _organizationsService = new OrganizationsService(ConfigurationManager.ConnectionStrings["TestAppConnectionString"].ConnectionString);
        }


        [HttpGet]
        public IHttpActionResult Check()
        {
            return Ok("Service is running at: " + DateTime.Now);
        }

        [HttpPost]
        [Route("api/collect/push")]
        public IHttpActionResult Push([FromBody]OrganizationPayload payload)
        {
            try
            {
                _log.Info("Initiating: " + DateTime.Now);
                IHttpActionResult response;

                var totalInsertedRows = _organizationsService.SaveOrganizations(payload);
                _log.Info($"{totalInsertedRows} were created");

                //insert to DB
                response = new ResponseMessageResult(new HttpResponseMessage(HttpStatusCode.OK));
                return response;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return InternalServerError();
            }
        }

        [HttpPost]
        [Route("api/collect/truncate")]
        public string Truncate([FromBody]string secret)
        {
            try
            {
                _log.Info("Attempting truncate: " + DateTime.Now);

                _organizationsService.TruncateOrganizations();
                _log.Info($"Organizations table was successfuly truncated");

                return "ok";
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return ex.Message;
            }
        }

        [HttpPost]
        [Route("api/organizations")]
        public HttpResponseMessage Post([FromBody]JObject payload)
        {
            if (payload == null || !payload.IsValid(_schema))
            {
                _log.Info($"Invalid payload received {payload}");
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new Exception($"Invalid payload received {payload}"));
            }
            try
            {
                _log.Info("Initiating insert organizations: " + DateTime.Now);

                if (_organizationsService.IsDatabaseInitialized())
                {
                    _log.Info("Organizations table contained existing rows, proceeding to table truncate.");
                    try
                    {
                        _organizationsService.TruncateOrganizations();
                        _log.Info($"Organization table was successfuly truncated");
                    }
                    catch (Exception ex)
                    {
                        _log.Error($"An error has ocurred while truncating organizations table: {ex.Message}");
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, new Exception($"An error has ocurred while truncating organizations table: {ex.Message}"));
                    }
                }

                var organizations = JsonConvert.DeserializeObject<OrganizationPayload>(payload.ToString(Formatting.None));

                var totalInsertedRows = _organizationsService.SaveOrganizations(organizations);
                _log.Info($"{totalInsertedRows} rows were successfuly created");

                var response = Request.CreateResponse(HttpStatusCode.OK, $"Success! {totalInsertedRows} rows were created");
                response.Headers.Add("X-Paging-TotalInsertedRowsCount", totalInsertedRows.ToString());
                return response;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, new Exception($"An error has occurred while performing the organization creation: {ex.Message}"));
            }
        }

        [HttpGet]
        [Route("api/organizations")]
        public HttpResponseMessage Search(string name, int? pageSize = 100, int? pageNo = 1)
        {
            try
            {
                var totalCount = 0;
                var pageCount = 0;

                // picks value from search and composes with default values if necessary
                var orgSearch = new OrganizationsSearch {
                    Name = name,
                    PageNo = pageNo,
                    PageSize = pageSize
                };

                var results = _organizationsService.Search(orgSearch);

                if (results.Any())
                {
                    totalCount = results.First().TotalCount.Value;
                    pageCount = GetPageCount(totalCount, orgSearch.PageSize.Value);
                }                    

                var resultJson = JsonConvert.SerializeObject(results, Formatting.None);

                var response = Request.CreateResponse(HttpStatusCode.OK, resultJson);
                response.Headers.Add("X-Paging-PageNo", orgSearch.PageNo.ToString());
                response.Headers.Add("X-Paging-PageSize", orgSearch.PageSize.ToString());
                response.Headers.Add("X-Paging-PageCount", pageCount.ToString());
                response.Headers.Add("X-Paging-TotalRecordCount", totalCount.ToString());
                return response;

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, new Exception($"An error has occurred while performing the search: {ex.Message}"));
            }            
        }

        private int GetPageCount(int numberOfRecords, int recordsPerPage)
        {
            int remainder;
            long result = Math.DivRem(numberOfRecords, recordsPerPage, out remainder);
            if ((remainder > 0) || (result == 0))
                result++;
            return (int)result;
        }

        [HttpDelete]
        [Route("api/organizations")]
        public HttpResponseMessage Delete()
        {
            try
            {
                _log.Info("Attempting truncate: " + DateTime.Now);
                _organizationsService.TruncateOrganizations();
                _log.Info($"Organizations table was successfuly truncated");

                var response = Request.CreateResponse(HttpStatusCode.OK, "Success! table Organization was truncted.");
                return response;
            }
            catch (Exception ex)
            {
                _log.Error($"An error has ocurred while truncating organizations table: {ex.Message}");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, new Exception($"An error has ocurred while truncating organizations table: {ex.Message}"));
            }
        }

    }
}