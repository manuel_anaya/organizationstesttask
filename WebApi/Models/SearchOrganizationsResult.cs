namespace WebApi.Models
{
    using Newtonsoft.Json;
    using System;
    
    public partial class SearchOrganizationsResult
    {
        [JsonProperty("org_name")]        
        public string RelatedNode { get; set; }

        [JsonProperty("relationship_type")]
        public string Relationship { get; set; }

        [JsonIgnore]
        public Nullable<int> TotalCount { get; set; }
    }
}
