﻿using System.Collections.Generic;
using System.Web.Http.ModelBinding;
using Newtonsoft.Json;

namespace WebApi.Models
{
    public class OrganizationPayload
    {
        [JsonProperty("org_name")]
        public string OrganizationName { get; set; }

        [JsonProperty("daughters")]
        public List<OrganizationPayload> Daughters { get; set; }
    }
}
