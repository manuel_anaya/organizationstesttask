﻿using WebApi.Helpers;

namespace WebApi.Models
{
    public class OrganizationsSearch
    {
        private static int _pageSize = 100;

        public string Name { get; set; }
        public int? PageSize { get; set; }
        public int? PageNo { get; set; }

        public static int DefaultPageSize()
        {
            return _pageSize;
        }

        public static int DefaultPage()
        {
            return _pageSize;
        }
    }
}